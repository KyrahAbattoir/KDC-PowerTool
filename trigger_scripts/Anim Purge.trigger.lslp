/****************************************************
Anim Purge.trigger.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Stops all animations.

Can break things but usually AOs recover fine from
this.

Can be put anywhere.
****************************************************/

default
{
    state_entry()
    {
        llSetMemoryLimit(16000);

        if(!llGetAttached())
        {
            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
            return;
        }

        llRequestPermissions(llGetOwner(),PERMISSION_TRIGGER_ANIMATION);
    }
    run_time_permissions(integer perms)
    {
        if((perms & PERMISSION_TRIGGER_ANIMATION) == PERMISSION_TRIGGER_ANIMATION)
        {
            list to_purge = llGetAnimationList(llGetOwner());
            integer i2;
            llOwnerSay((string)llGetListLength(to_purge) + " animation(s) to purge...");

            for(i2 = 0;i2 < llGetListLength(to_purge);i2++)
                llStopAnimation(llList2Key(to_purge,i2));

            llOwnerSay((string)llGetListLength(to_purge) + "...done!");
        }

        llSetScriptState(llGetScriptName(),FALSE);
        llResetScript();
    }
}
