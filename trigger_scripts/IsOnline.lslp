/****************************************************
IsOnline.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

You need a notecard "IsOnline.config" with the
Avatars you want to scan

Check the sample config file.
*****************************************************/

integer nLine = -1;
string card_name;

key card_id;
key status_id;
string l_name;

string NOTECARD = "IsOnline.config";

default
{
	state_entry()
	{
        llSetMemoryLimit(16000);

        llOwnerSay("Online scan started.");
        nLine = 0;
        card_id = llGetNotecardLine(NOTECARD,nLine);
	}
	dataserver(key id,string data)
    {
        if(id == card_id)
        {
            if(data != EOF)
            {
                list tmp = llParseString2List(data,[" "],[]);
                if(llGetListLength(tmp) == 3)
                {
                    l_name = llList2String(tmp,1) + " " + llList2String(tmp,2);
                    status_id = llRequestAgentData((key)llList2String(tmp,0),DATA_ONLINE);
                }
                else //bogus line, skipping
                {
                    nLine++;
                    card_id = llGetNotecardLine(NOTECARD,nLine);
                }
            }
            else
            {
                card_id = "";
                status_id = "";
                l_name = "";
                llOwnerSay("Online scan finished.");

                llSetScriptState(llGetScriptName(),FALSE);
                llResetScript();
            }
        }
        else if(id == status_id)
        {
            if((integer)data == TRUE)
                llOwnerSay(l_name + " is online.");

            nLine++;
            card_id = llGetNotecardLine(NOTECARD,nLine);
        }
    }
}
