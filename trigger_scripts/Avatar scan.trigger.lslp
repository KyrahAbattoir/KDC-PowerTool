/****************************************************
Avatar scan.trigger.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

List all Agents in the region sorted by complexity.

JOHN DOE DW:43513 SC:83(86) 4327030kb TIME:0.128ms STREAM:95 <19,11,41> en
NAME     COMPLEX.  SCRIPTS   MEMORY       SCRIPT    SERVER   POSITION  LANGUAGE
                  +(TOTAL)                TIME      STREAM
                                                     COST

Can be put anywhere
****************************************************/

//This is by default by draw weight, but you can use any of the supported (integer) values:
//http://wiki.secondlife.com/wiki/LlGetObjectDetails
integer giSORTING_ELEMENT = OBJECT_RENDER_WEIGHT;

default
{
    state_entry()
    {
        llSetMemoryLimit(24000); //seem to work fine with 38 agents at least, data use per avatar should be relatively constant.

        list avs = llGetAgentList( AGENT_LIST_REGION,[]);

        integer entry = llGetListLength(avs);
        while(entry--)
            avs = llListInsertList(avs,llGetObjectDetails(llList2Key(avs,entry),[giSORTING_ELEMENT]),entry);
        avs = llListSort(avs,2,TRUE);

        entry = llGetListLength(avs);
        llOwnerSay((string)(entry/2)+" agents in region.");
        llOwnerSay("=================");
        while(entry-=2)
        {
            key avatar = llList2Key(avs,entry+1);
            list details = ["secondlife:///app/agent/"+(string)avatar+"/about"];
            details += llGetObjectDetails(avatar,[  OBJECT_RENDER_WEIGHT,OBJECT_RUNNING_SCRIPT_COUNT,OBJECT_TOTAL_SCRIPT_COUNT,
                                                    OBJECT_SCRIPT_MEMORY,OBJECT_SCRIPT_TIME,OBJECT_STREAMING_COST,OBJECT_POS,
                                                    39,40]);//OBJECT_ANIMATED_COUNT,OBJECT_ANIMATED_SLOTS_AVAILABLE

            //basic/premium detection
            if((llList2Integer(details,9) + llList2Integer(details,10)) == 2)
                details = llListReplaceList(details,[" [P]"],8,9);
            else
                details = llListReplaceList(details,[" [B]"],8,9);

            details += " "+llGetAgentLanguage(avatar);


            //This is all shitty unreadable formatting, I know.

            vector pos = llList2Vector(details,7);
            details = llListReplaceList(details,["<"+(string)((integer)pos.x)+","+(string)((integer)pos.y)+","+(string)((integer)pos.z)+">"],7,7);//clean that position vector a bit.
            details = llListInsertList(details,[" "],7);
            details = llListReplaceList(details,[llList2Integer(details,6)],6,6);//streaming cost has useless decimals
            details = llListInsertList(details,["ms STREAM:"],6);
            details = llListReplaceList(details,[llGetSubString((string)(llList2Float(details,5)*1000),0,4)],5,5);//time to ms
            details = llListInsertList(details,["kb TIME:"],5);
            details = llListInsertList(details,[") "],4);
            details = llListInsertList(details,["("],3);
            details = llListInsertList(details,[" SC:"],2);
            details = llListInsertList(details,[" DW:"],1);

            llOwnerSay(llDumpList2String(details,""));//Done, output!
        }
        llOwnerSay("=================");

        llSetScriptState(llGetScriptName(),FALSE);
        llResetScript();
    }
}
