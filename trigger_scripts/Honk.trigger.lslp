/****************************************************
IsOnline.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Super simple trigger, it plays a random "honk" sound.

It uses the sound API call so put it in the HUD.
*****************************************************/

$import API.lslm () API_;

list glSounds = [
"fc0ebf50-2acc-b018-b2c3-7a190e6f27e1",
"2db0a379-f14b-e597-0a92-c581fa7bd58b",
"be8fceb6-3252-27d1-3f8b-8bf9cbf2cc8f"
];

default
{
    state_entry()
    {
        llSetMemoryLimit(8000);

        integer iSound = (integer)llFrand(llGetListLength(glSounds));
        API_TriggerSound(llList2String(glSounds,iSound),1.0);

        llSetScriptState(llGetScriptName(),FALSE);
        llResetScript();
    }
}
