/****************************************************
Footstep.addon.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Generic heel clicking/footsep sound emitter, uses
sound loops so not really speed sensitive.

Has to be in the player, because of the sounds.

Check the "FootStep.config" sample notecard for
configuration.
*****************************************************/

$import API.lslm () API_;
$import _MenuMaker.lslm () MENU_;
$import _timer.lslm () TIMER_;

string CALL_BUTTON = "Step sound";

string NOTECARD = "FootStep.config";

integer is_walking = FALSE;
integer is_over_ground = FALSE;
string curr_snd = "";

float next_sound_trigger = 0;
float next_sound_delay = 0.5;

float next_material_change = 0;
float next_material_change_delay = 1.0;

list presets;
list tmp_buffer;

string snd_loop_softground;
string snd_loop_hardground;
string snd_start_softground;
string snd_start_hardground;
string snd_stop_softground;
string snd_stop_hardground;
string snd_land_softground;
string snd_land_hardground;
float snd_loop_softground_vol;
float snd_loop_hardground_vol;
float snd_start_softground_vol;
float snd_start_hardground_vol;
float snd_stop_softground_vol;
float snd_stop_hardground_vol;
float snd_land_softground_vol;
float snd_land_hardground_vol;
integer PRESET_LENGTH = 17;

integer nLine = 0;
key preset_card = NULL_KEY;
integer current_preset = 0;

LoadPreset(integer index)
{
    current_preset = index;
    llStopSound();

    llOwnerSay("Walk sound preset '" + llList2String(presets,index++) + "' loaded successfully.");
    snd_loop_softground         = llList2String(presets,index++);
    snd_loop_hardground         = llList2String(presets,index++);
    snd_start_softground        = llList2String(presets,index++);
    snd_start_hardground        = llList2String(presets,index++);
    snd_stop_softground         = llList2String(presets,index++);
    snd_stop_hardground         = llList2String(presets,index++);
    snd_land_softground         = llList2String(presets,index++);
    snd_land_hardground         = llList2String(presets,index++);
    snd_loop_softground_vol     = llList2Float(presets,index++);
    snd_loop_hardground_vol     = llList2Float(presets,index++);
    snd_start_softground_vol    = llList2Float(presets,index++);
    snd_start_hardground_vol    = llList2Float(presets,index++);
    snd_stop_softground_vol     = llList2Float(presets,index++);
    snd_stop_hardground_vol     = llList2Float(presets,index++);
    snd_land_softground_vol     = llList2Float(presets,index++);
    snd_land_hardground_vol     = llList2Float(presets,index);
}

TriggerSound(key snd,float vol)
{
    if(snd != "" && snd != NULL_KEY && vol > 0.0)
        llTriggerSound(snd,vol);
}
LoopSound(key snd,float vol)
{
    if(snd != "" && snd != NULL_KEY && vol > 0.0)
        llLoopSound(snd,vol);
}

RequestPermissions()
{
    llCollisionSound("",0.0);
    llRequestPermissions(llGetOwner(),PERMISSION_TAKE_CONTROLS);
}

list PadList(list in,integer length)
{
    while( llGetListLength(in) < length )
        in += [""];

    return in;
}

TakeControls()
{
    llTakeControls(CONTROL_LEFT|CONTROL_RIGHT|CONTROL_FWD|CONTROL_BACK,TRUE,TRUE);
}

list FetchPresets()
{
    list a;
    integer i;
    for(i=0;i<llGetListLength(presets);i+=PRESET_LENGTH)
        a += llList2List(presets,i,i);
    return a;
}

integer TIMER_MENU = 5655;
string BTN_BACK   = "<BACK>";
OpenMenu(integer page)
{
	MENU_Create(llGetOwner(),MENU_MakePage([BTN_BACK]+FetchPresets(),page),"Choose a preset",0,page);
}

key read_notecard;
CheckConfig()
{
    if( llGetInventoryKey(NOTECARD) == preset_card )
        return;

    tmp_buffer = [];
    presets = [];
    preset_card = llGetInventoryKey(NOTECARD);
    nLine = 0;
    read_notecard = llGetNotecardLine(NOTECARD,nLine++);
}

//pragma inline
Addon_Start()
{
    API_RegisterButton(CALL_BUTTON);
    CheckConfig();

    if(llGetAttached())
        RequestPermissions();

    llOwnerSay("Footstep addon started.");
}
//pragma inline
Addon_Stop()
{
    API_UnRegisterButton(CALL_BUTTON);
    MENU_Destroy();
    TIMER_ClearTimer(TIMER_MENU);
    llReleaseControls();

    llOwnerSay("Footstep addon stopped.");
}

default
{
    state_entry()
    {
        Addon_Start();
    }
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_STOP && str == llGetScriptName())
        {
            Addon_Stop();

            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
        }
        if(num == API_SVC_BUTTON_CALL && str == CALL_BUTTON)
            OpenMenu(0);
        if(num == API_SVC_RESET_MENU)
            API_RegisterButton(CALL_BUTTON);
    }
    run_time_permissions(integer perms)
    {
        if(perms & PERMISSION_TAKE_CONTROLS)
            TakeControls();
    }
    changed(integer change)
    {
        if( change & CHANGED_INVENTORY && llGetInventoryKey(NOTECARD) != preset_card)
            CheckConfig();
    }
    timer()
    {
        if(TIMER_IsItTime(TIMER_MENU) && TIMER_IsTimerPresent(TIMER_MENU))
        {
            MENU_Destroy();
            TIMER_ClearTimer(TIMER_MENU);
        }
    }
    listen(integer channel,string name,key id,string data)
    {
        if(channel != MENU_channel || id != llGetOwner())
            return;

        MENU_Destroy();
        TIMER_ClearTimer(TIMER_MENU);

        if(data == BTN_BACK)
            API_OpenRootMenu();
        else if(data == MENU_BTN_PREV())
            OpenMenu(MENU_GetB() - 1);
        else if(data == MENU_BTN_NEXT())
            OpenMenu(MENU_GetB() + 1);
        else
        {
            integer index = llListFindList(presets,[data]);
            if(index != -1)
                LoadPreset(index);
            OpenMenu(MENU_GetB());
        }
    }
    dataserver(key queryid,string data)
    {
        if(queryid != read_notecard)
            return;

        if(data != EOF)
        {
            list ldata = llParseString2List(data,[" "],[]);
            string command = llList2String(ldata,0);
            data = llList2String(ldata,2);

            if(command == "new_walk")
            {
                //if the buffer is non empty, we push it into the preset list.
                if( llGetListLength(tmp_buffer) > 0 )
                    presets += tmp_buffer;

                //then we overwrite it with blank data
                tmp_buffer = PadList([data],PRESET_LENGTH);
            }
            else if( llGetListLength(tmp_buffer) > 0 )
            {
                //if the buffer is non empty, we try to fill it up
                list commands = ["\n\n",   "loop.soft","loop.hard","start.soft","start.hard","stop.soft","stop.hard","land.soft","land.hard",
                                           "loop.soft.volume","loop.hard.volume","start.soft.volume","start.hard.volume","stop.soft.volume",
                                           "stop.hard.volume","land.soft.volume","land.hard.volume"];
                integer b_index = llListFindList(commands,[command]);
                if(b_index > 0)
                    tmp_buffer = llListReplaceList(tmp_buffer,[data],b_index,b_index);
            }
            read_notecard = llGetNotecardLine(NOTECARD,nLine++);
        }
        else
        {
            //if the buffer is non empty, we pushit into the preset list.
            if( llGetListLength(tmp_buffer) > 0 )
                presets += tmp_buffer;

            if( llGetListLength(presets) < (PRESET_LENGTH * (current_preset+1)) )
                LoadPreset(0);
            else
                LoadPreset(current_preset);
        }
    }
    attach(key id)
    {
        if(id == NULL_KEY)
            llReleaseControls();
        else
            RequestPermissions();
    }
    control(key id,integer level,integer edge)
    {
        integer agent_info = llGetAgentInfo(llGetOwner());

        if( (agent_info & AGENT_WALKING) == AGENT_WALKING )
            is_walking = TRUE;

        if( !level || ( (agent_info & AGENT_IN_AIR) == AGENT_IN_AIR ) || ( (agent_info & AGENT_SITTING) == AGENT_SITTING ) )
            is_walking = FALSE;
        if(llGetAnimation(llGetOwner()) == "CrouchWalking")
            is_walking = FALSE;

        if(!is_walking && curr_snd != "")
        {
            curr_snd = "";
            llStopSound();

            if(!is_over_ground)
                TriggerSound(snd_stop_hardground,snd_stop_hardground_vol);
            else
                TriggerSound(snd_stop_softground,snd_stop_softground_vol);
        }
        if(is_walking)
        {
            if(is_over_ground && curr_snd != snd_loop_softground)
            {
                if(next_sound_trigger + next_sound_delay > llGetTime() )
                    return;
                next_sound_trigger = llGetTime();

                llStopSound();
                TriggerSound(snd_stop_softground,snd_stop_softground_vol);
                LoopSound(snd_loop_softground,snd_loop_softground_vol);
                curr_snd = snd_loop_softground;
            }
            else if(!is_over_ground && curr_snd != snd_loop_hardground)
            {
                if(next_sound_trigger + next_sound_delay > llGetTime() )
                    return;
                next_sound_trigger = llGetTime();

                llStopSound();
                TriggerSound(snd_stop_hardground,snd_stop_hardground_vol);
                LoopSound(snd_loop_hardground,snd_loop_hardground_vol);
                curr_snd = snd_loop_hardground;
            }
        }
    }
    collision(integer num_detected)
    {
        if( (next_material_change + next_material_change_delay) < llGetTime() && is_over_ground)
        {
            next_material_change = llGetTime();
            is_over_ground = FALSE;
        }
        string anm = llGetAnimation(llGetOwner());
        if( anm == "Landing" || anm == "Soft Landing")
        {
            if(next_sound_trigger + next_sound_delay > llGetTime() )
                return;
            next_sound_trigger = llGetTime();
            TriggerSound(snd_land_hardground,snd_land_hardground_vol);
        }
    }
    land_collision(vector pos)
    {
        if( (next_material_change + next_material_change_delay) < llGetTime() && !is_over_ground)
        {
            next_material_change = llGetTime();
            is_over_ground = TRUE;
        }
        string anm = llGetAnimation(llGetOwner());
        if( anm == "Landing" || anm == "Soft Landing")
        {
            if(next_sound_trigger + next_sound_delay > llGetTime() )
                return;
            next_sound_trigger = llGetTime();
            TriggerSound(snd_land_softground,snd_land_softground_vol);
        }
    }
}
