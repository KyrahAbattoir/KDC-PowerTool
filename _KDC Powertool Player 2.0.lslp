/****************************************************
_KDC Powertool Player 2.0.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

This is the "Player" portion of the powertool
Put it in something you wear.

Use this for scripts that play sounds and can't
afford the latency/potential packet lost messages of
forwarding commands through a HUD->PLAYER command.
****************************************************/

$import API.lslm () API_;
$import _MenuMaker.lslm () MENU_;
$import _timer.lslm () TIMER_;

string TRIGGER  = ".trigger";
string ADDON  = ".addon";
list triggers;
list services;
list extra_buttons;

ResyncAddons()
{
    triggers = [];
    services = [];

    integer i;
    integer max = llGetInventoryNumber(INVENTORY_SCRIPT);
    for(i=0;i<max;i++)
    {
        string scriptname = llGetInventoryName(INVENTORY_SCRIPT,i);
        if(llGetSubString(scriptname,-8,-1) == TRIGGER)
            triggers += [llDeleteSubString(scriptname,-8,-1)];
        else if(llGetSubString(scriptname,-6,-1) == ADDON)
            services += [llDeleteSubString(scriptname,-6,-1)];
    }
    llMessageLinked(LINK_THIS,API_SVC_RESET_MENU,"","");
}

string BTN_SERVICES = "<ADDONS>";
string BTN_HUD = "<BACK>";
string BTN_BACK   = "<BACK>";
integer TIMER_MENU = 5655;

integer MENU_CONTEXT_ROOT = 0;
integer MENU_CONTEXT_SERVICES = 1;

RootMenu(integer page)
{
    integer i;
    integer max = llGetListLength(services);
    integer running;
    for(i=0;i<max;i++)
    {
        string script = llList2String(services,i);
        if(llGetScriptState(script+ADDON))
            running++;
    }
    list details = llGetObjectDetails(llGetKey(),[OBJECT_SCRIPT_MEMORY,OBJECT_RUNNING_SCRIPT_COUNT,OBJECT_TOTAL_SCRIPT_COUNT]);
    integer memory = llList2Integer(details,0);
    integer scripts = llList2Integer(details,1);
    integer total = llList2Integer(details,2);

    string message =   "Player menu:\n\Addons running: "+(string)running+"/"+(string)max+
                       "\nMemory used: "+(string)(memory/1000)+"Kb"+
                       "\nScripts: "+(string)scripts+"/"+(string)total;
    MENU_Create(llGetOwner(),MENU_MakePage([BTN_SERVICES,BTN_HUD]+extra_buttons,page),message,MENU_CONTEXT_ROOT,page);
    TIMER_SetTimer(TIMER_MENU,120.0);
}

ServiceMenu(integer page)
{
    list service_names;
    integer max = llGetListLength(services);
    integer i;
    for(i=0;i<max;i++)
    {
        string script = llList2String(services,i);
        if(llGetScriptState(script+ADDON))
            service_names += ["☑"+script];
        else
            service_names += ["☐"+script];
    }
    MENU_Create(llGetOwner(),MENU_MakePage([BTN_BACK]+service_names,page),"Toggle addons on and off:",MENU_CONTEXT_SERVICES,page);
    TIMER_SetTimer(TIMER_MENU,120.0);
}

default
{
    state_entry()
    {
        llListen(API_CommChannel,"","","");
        ResyncAddons();
    }
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_ADD_MENU)
        {
            if(llListFindList(extra_buttons,[str]) != -1)
                return;
            extra_buttons+=[str];
        }
        else if(num == API_SVC_REM_MENU)
        {
            integer index = llListFindList(extra_buttons,[str]);
            if(index == -1)
                return;
            extra_buttons = llDeleteSubList(extra_buttons,index,index);
        }
        else if(num == API_ROOT_MENU)
            RootMenu(0);
    }
    listen(integer channel,string name,key id, string data)
    {
        if(id != llGetOwner() && llGetOwnerKey(id) != llGetOwner())
            return;

        if(channel == API_CommChannel)
        {
            list message = llCSV2List(data);
            integer idx;
            if(llList2String(message,idx++) != API_CMD_HEADER)
                return;

            string command = llList2String(message,idx++);
            if(command == API_CMD_PLAYERMENU)
                RootMenu(0);
            else if(command == API_CMD_PLAYSOUND || command == API_CMD_TRIGGERSOUND)
            {
                key sound = llList2String(message,idx++);
                float volume = llList2Float(message,idx++);

                if(command == API_CMD_PLAYSOUND)
                    llPlaySound(sound,volume);
                else
                    llTriggerSound(sound,volume);
            }
        }
        else if(channel == MENU_channel)
        {
            MENU_Destroy();
            TIMER_ClearTimer(TIMER_MENU);
            integer context = MENU_GetA();
            if(context == MENU_CONTEXT_ROOT)
            {
                if(data == BTN_HUD)
                    API_OpenHud();
                else if(data == BTN_SERVICES)
                    ServiceMenu(0);
                else if(llListFindList(extra_buttons,[data]) != -1)
                    API_CallButton(data);
            }
            else if(context == MENU_CONTEXT_SERVICES)
            {
                if(data == MENU_BTN_PREV())
                {
                   integer page = MENU_GetB()-1;
                   if(page <0)
                       page = 0;
                    ServiceMenu(page);
                }
                else if(data == MENU_BTN_NEXT())
                    ServiceMenu(MENU_GetB()+1);
                else if(data == BTN_BACK)
                    RootMenu(0);
                else
                {
                    data = llDeleteSubString(data,0,0);

                    //if(llListFindList(services,[data]) == -1);
                    //    return;

                    string scriptname = data+ADDON;
                    if(llGetScriptState(scriptname))
                        API_Service_Stop(scriptname);
                    else
                        llSetScriptState(scriptname,TRUE);

                    llSleep(1.0); //so the menu doesn't reload before the script has stopped.
                    ServiceMenu(MENU_GetB());
                }
            }
        }
    }
}
