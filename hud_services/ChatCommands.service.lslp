/****************************************************
ChatCommands.service.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

This service is literally just a chat command parser
it's all on channel /1
The only reason it's a service is so you can easily
shut it down... for reasons.

whois <uuid> : prints the name this uuid belongs to.

whoisown <uuid> : prints the name of this uuid's owner.

sb64 <string>: string to base64

ib64 <string>: integer to base64

b64s <string>: base64 to string

b64i <string>: base64 to integer

inspect <Firstname> <Lastname> (or)
inspect Username : dumps all attached object names and
their creator, script count & LI.

Can be put anywhere.
*****************************************************/

$import API.lslm () API_;
$import _MenuMaker.lslm () MENU_;
$import _timer.lslm () TIMER_;

integer handle;

integer MEM_DEFAULT = 16000;

//pragma inline
Service_Start()
{
    handle = llListen(1,"",llGetOwner(),"");
    llOwnerSay("Chat Command service started.");
}
//pragma inline
Service_Stop()
{
    llListenRemove(handle);
    llOwnerSay("Chat Command service stopped.");
}

default
{
	state_entry()
	{
		llSetMemoryLimit(MEM_DEFAULT);
		Service_Start();
	}
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_STOP && str == llGetScriptName())
        {
            Service_Stop();

            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
        }
    }
    listen(integer channel,string name,key id,string message)
    {
        if(llGetSubString(message,0,5) == "whois ")
        {
            string k = llDeleteSubString(message,0,5);
            llOwnerSay("secondlife:///app/agent/"+k+"/inspect");
        }
        else if(llGetSubString(message,0,8) == "whoisown ")
        {
            string k = llGetOwnerKey((key)llDeleteSubString(message,0,8));
            llOwnerSay("secondlife:///app/agent/"+k+"/inspect");
        }
        else if(llGetSubString(message,0,4) == "sb64 ")
        {
            string k = llDeleteSubString(message,0,4);
            llOwnerSay(llStringToBase64(k));
        }
        else if(llGetSubString(message,0,4) == "ib64 ")
        {
            string k = llDeleteSubString(message,0,4);
            llOwnerSay(llIntegerToBase64((integer)k));
        }
        else if(llGetSubString(message,0,4) == "b64s ")
        {
            string k = llDeleteSubString(message,0,4);
            llOwnerSay(llBase64ToString(k));
        }
        else if(llGetSubString(message,0,4) == "b64i ")
        {
            string k = llDeleteSubString(message,0,4);
            llOwnerSay((string)llBase64ToInteger(k));
        }
        else if(llGetSubString(message,0,7) == "inspect ")
        {
            string k = llDeleteSubString(message,0,7);

            list avatars_in_region = llGetAgentList(AGENT_LIST_REGION,[]);
            integer i;
            integer max = llGetListLength(avatars_in_region);
            key avkey = NULL_KEY;
            for(i=0;i<max && avkey == NULL_KEY;i++)
            {
                key av = llList2Key(avatars_in_region,i);
                if(k == llKey2Name(av))
                  avkey = av;
                else if(k == llGetUsername(av))
                  avkey = av;
                else if(k == llGetDisplayName(av))
                  avkey = av;
            }

            if(avkey == NULL_KEY)
            {
               llOwnerSay("Avatar "+k+" not found in region.");
               return;
            }

            llSetMemoryLimit(MEM_DEFAULT*2);//Can cause crashes otherwise.
            list attachments = llGetAttachedList(avkey);
            max = llGetListLength(attachments);
            llOwnerSay("Attachments worn by "+k+":");
            llOwnerSay("----------------------------------------------------");
            for(i=0;i<max;i++)
            {
                key attachment = llList2Key(attachments,i);
                string atname = llKey2Name(attachment);
                list data = llGetObjectDetails(attachment,[OBJECT_CREATOR,OBJECT_TOTAL_SCRIPT_COUNT,OBJECT_PRIM_EQUIVALENCE,24]);//RENDER WEIGHT
                key creator = llList2Key(data,0);
                llOwnerSay(atname + " by secondlife:///app/agent/"+llList2String(data,0)+"/inspect scripts:"+llList2String(data,1)+
                              " LI:"+llList2String(data,2));
            }
            llOwnerSay("----------------------------------------------------");
            llSetMemoryLimit(MEM_DEFAULT);
        }
    }
}
