/****************************************************
Antifacelight.service.lslp

Copyright (c) 2016-2020, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Scans people entering the current parcel and notifies
them if they are wearing a recognized facelight or
reflector system.

*****************************************************/
$import API.lslm () API_;

list fingerprints = [
"1f13f3f54311d235f15a9727bb821c39d533a69d",//Licht T6 v3T Avatar Mitte V1 by o0Melina0o
"bac21139646b5292f5143f2735f957fda9dffb64",//Reflection Lights (Wear) by Veronicca VanDouser
"6fb3730d6db2afb7e13978daee8c37d9a1fa0759",//[GA.EG] Face Lights by Gael Streeter
"d5efff1b48440a6987fcc15273f57c66385db20d",//Soft Body Illuminator (for material clothing, add) by Freya Olivieri
"4817c8055a606505100bf144296a68a97afb79fc",//ShinyLights 2.3 [HDRi] (avatar center) by Raelyn Beatty (keira.beatty)
"fecc4d0e3e880e982d4505376399e052602856c1",//Optional Light to see Materials. by Fhara K. Ricielli (fhara.acacia)
"92897eee3ac840fb678b4179f5ac96a23d496774",//SQ's Environment Box hidden frames v.1.4 (Avatar Center) by Ann (transann)
"14853925c359ba841f3a475e21f369357e1735bb",//*XO* Shiny Leather & Latex Light by Cosmeja
"af5ff5ca4ee3988baf620927633a0fc9bd2ba4ca",//[DHB] body light   (add me!) 1 by wanglulu1989
"a670689e1e0cfad961e9c92fac2bb6261da8ba6c",//.:: DELISH ::. Facelight (mouth) by Cognitive Gears
"22abb34129a84c671779d53c0a5a447ae0981fdb",//rezology Lusterlight (optional) by Selc
"be5e4a06cd57361ad6c31b31b8f74801c0c1749b",//LAQ ~ Face light by Mallory Cowen
"22abb34129a84c671779d53c0a5a447ae0981fdb",//rezology Lusterlight (optional) by Selc
//"f503711a056443241330882fc9a4fd18d8761107",//blond pretty hair by Jhoyce Xue (no this is not a mistake, the entire linkset is a light)
"b05c4bb920f924cbadd7adbc1d70632391a92a41",//Latex_Light by Alandrea Berboral
"ad33c8424c379f29745715df625d190027f2ae73",//Rotating Lighting Boxes_sky_hollow_lessSaturated_dim by Celestine
"6f75d821ccdb2df3c2cf39e4f1f4d617609e34c1",//Light Projector by Mei (mei.avril)
"ed2378552d6b41767db1a642003cc1f79dedcdea",//Shiny Latex Reflector [Box] (Stomach) by UberFoXy
"7b2b01dab0f5e03154171a1aa1e8226f0fd285d3",//H@S Specular Lights (add me) by Saki Galiazzo
"a2201458cadc3de2871e827631ce34de826c7445",//Env Box ~ Workshop (Indoors, Bright) (root) by Marine (marine.kelley)
"56958866060f386d2a324a00e4f95ee12a9c72ca",//CATWA Face Light [Low] by Catwa.Clip (catwa.clip
"d74d7cccd46c8dfb79a42bdb519106d6c287a1d2",//LeLutka.Facelight-wear me by Jaden Art (jadenart)
"e452b736926cbe75d6533c4176cd35b6ff89f582",//Spinning lights (root) by Marine (marine.kelley)
"f28fbb1f87fd5a6c1a83e4765a6cd62b9690edec",//Sugar Pie Facelight II (Chin) by Edie Shoreland
"f1cd6fc498b88bfc6b02c7c68d12d125b8a41939",//(wear)bodylight by clblue
"20896872b5f43a5c9d20d7df37eb78dd408117df",//[DHB]Body lighti 2.0 !!!! (add me) by wanglulu1989
"e7154af5cb2807a7eaffc404919b9dfef79cbe40",//latexlight by Lizzi Klaar
"3c09641f2e15538393a687aa01f2466d6eaa4345",//Face & Body Light by Istephanija Munro
"57251cf0ae187732fcbc0b9632bd59d64d626070",//Reflector by Haru (pianolily)
"b9e5e56d0a64ce904ad967648a9be162d77c17d4",//CSC - Full Body Light - 1.1 by KimmyQuay Twine
"7986b62df243396b03bfa1ba05b996d33fbc8fe7",//Face Light  (wear me) by Bongo (bongo.steampunk)
"56d322a2840e5f0d6e0dcc4a3685ddf266f20f59",//Lara Reflector Dull (update) by Gummi Gloss (motoko.enyo)
"65fc7c793334605f7014360af6dd859e390ddadf"//BioDoll 2.0 Light Reflection by Kit (pixxieboy)
];

list nearby_avatars;

//pragma inline
agent_added(key avatar)
{
    list objects = llGetAttachedList(avatar);
    integer entry = llGetListLength(objects);

    string found;
    while(entry--)
    {
        key object = llList2Key(objects,entry);
        list data = llGetObjectDetails(object,[OBJECT_NAME,OBJECT_CREATOR,OBJECT_CREATION_TIME]);

        string name     = llList2String(data,0);
        string creator  = llList2Key(data,1);
        string date     = llList2String(data,2);
        string fingerprint = llSHA1String(creator+date);

        if(llListFindList(fingerprints,[fingerprint]) != -1)
            found += name + " by secondlife:///app/agent/"+creator+"/about\n";
    }
    if(found)
    {
        string name = llGetObjectName();
        llSetObjectName(":");

        string message = "/me Hello secondlife:///app/agent/"+(string)avatar+"/about, please take a moment to remove your face/lighting/reflection attachments, they over saturate the scene and make you look less beautiful than you normally would. ^_^\nDetected lights:\n";
        message += found;
        message += "Thank you for your understanding.";
        llOwnerSay("Antifacelight.service: notified "+llKey2Name(avatar));
        llRegionSayTo(avatar,0,message);

        llSleep(5.0);
        llSetObjectName(name);
    }
}

//pragma inline
agent_removed(key avatar)
{
    //nuffin
}

//pragma inline
Service_Start()
{
    llSetTimerEvent(60.0);
    llOwnerSay("Anti-Facelight service started");
}
//pragma inline
Service_Stop()
{
    llSetTimerEvent(0.0);
    nearby_avatars = [];
    llOwnerSay("Anti-Facelight service stopped");
}

default
{
    state_entry()
    {
        //llSetMemoryLimit(8000);
        Service_Start();
    }
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_STOP && str == llGetScriptName())
        {
            Service_Stop();

            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
        }
    }
    timer()
    {
        list agents = llGetAgentList(AGENT_LIST_REGION,[]);

        //add new avatars;
        integer entry = llGetListLength(agents);
        while(entry--)
        {
            key agent = llList2Key(agents,entry);
            vector agent_position = llList2Vector(llGetObjectDetails(agent,[OBJECT_POS]),0);

            if(llVecDist(agent_position,llGetPos()) > 20.0)//no need to annoy the entire region.
                jump continue;

            if(llListFindList(nearby_avatars,[agent]) != -1)
                jump continue;
            //could add finer conditions there.

            nearby_avatars += agent;
            agent_added(agent);
            @continue;
        }

        //remove avatars that are gone;
        entry = llGetListLength(nearby_avatars);
        while(entry--)
        {
            key agent = llList2Key(nearby_avatars,entry);
            if(llListFindList(agents,[agent]) == -1)
            {
                nearby_avatars = llDeleteSubList(nearby_avatars,entry,entry);
                agent_removed(agent);
            }
        }
    }
}
