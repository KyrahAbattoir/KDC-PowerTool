/****************************************************
Sample.service.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Simple sample service, it starts, stops, and it also
injects a button on the main menu.
*****************************************************/

$import API.lslm () API_;
$import _MenuMaker.lslm () MENU_;
$import _timer.lslm () TIMER_;

string CALL_BUTTON = "Sample";

//pragma inline
Service_Start()
{
    API_RegisterButton(CALL_BUTTON);
    llOwnerSay("Sample service started");
}
//pragma inline
Service_Stop()
{
    API_UnRegisterButton(CALL_BUTTON);
    llOwnerSay("Sample service stopped");
}

default
{
    state_entry()
    {
        llSetMemoryLimit(8000);
        Service_Start();
    }
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_STOP && str == llGetScriptName())
        {
            Service_Stop();

            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
        }
        if(num == API_SVC_RESET_MENU)
        {
            //The core script is asking to re authenticate our buttons
            API_RegisterButton(CALL_BUTTON);
        }
        if(num == API_SVC_BUTTON_CALL && str == CALL_BUTTON)
        {
            llOwnerSay("Sample service button pressed.");
            API_OpenRootMenu();
        }
    }
}
