/****************************************************
AfkAttach.service.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Simple service that uses RLV to auto-attach a folder
(#RLV/AFK) when your avatar goes in away mode.

This requires a RLV compatible client obviously.
*****************************************************/

$import API.lslm () API_;

//pragma inline
Service_Start()
{
	llSetTimerEvent(5.0);
    llOwnerSay("AFK Attach service started");
}
//pragma inline
Service_Stop()
{
    LeaveAFK();
    llSetTimerEvent(0.0);
    llOwnerSay("AFK Attach service stopped");
}

integer is_afk;
EnterAFK()
{
    if(is_afk)
        return;
    is_afk = TRUE;

    //This part is for M3 heads if you use it.
    //llRegionSayTo(llGetOwner(),-34525470,"LEye:Lids:4");
    //llRegionSayTo(llGetOwner(),-34525470,"REye:Lids:4");

    llOwnerSay("@attachover:AFK=force");
}
LeaveAFK()
{
    if(!is_afk)
        return;
    is_afk = FALSE;

    //This part is for M3 heads if you use it.
    llRegionSayTo(llGetOwner(),-34525470,"LEye:Lids:2");
    llRegionSayTo(llGetOwner(),-34525470,"REye:Lids:2");

    llOwnerSay("@detachall:AFK=force");
}

default
{
    state_entry()
    {
        llSetMemoryLimit(8000);
        Service_Start();
    }
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_STOP && str == llGetScriptName())
        {
            Service_Stop();

            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
        }
    }
    timer()
    {
        integer away = llGetAgentInfo( llGetOwner() ) & AGENT_AWAY;
        integer sitting = llGetAgentInfo( llGetOwner() ) & AGENT_SITTING;

        if(away && !sitting)
            EnterAFK();
        else if(!away)
            LeaveAFK();
    }
}
