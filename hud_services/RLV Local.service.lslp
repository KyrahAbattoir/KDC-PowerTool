/****************************************************
RLV Local.service.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

Somewhat crude system to force people to talk to you
in local chat instead of IMing you.

When the service is running, anyone in local chat
range is automatically blocked from sending you an IM.

There is no proper RLV detection implemented in this
because lazy.

Can be put anywhere.
*****************************************************/

$import API.lslm () API_;
$import _MenuMaker.lslm () MENU_;
$import _timer.lslm () TIMER_;

list whitelist = [
//You can exclude people from the IM restriction by adding their UUID to this list.
];

//pragma inline
Service_Start()
{
    llOwnerSay("RLV Local chat service started.");
    Start_RLV();
}
//pragma inline
Service_Stop()
{
    integer i;
    for(;i<llGetListLength(whitelist);i++)
        llOwnerSay("@recvim:"+llList2String(whitelist,i)+"=rem");

    llOwnerSay("@recvim:20=y");
    llOwnerSay("RLV Local chat service stopped.");
}

//pragma inline
Start_RLV()
{
    llOwnerSay("@recvim:20=n");
    integer i;
    for(;i<llGetListLength(whitelist);i++)
        llOwnerSay("@recvim:"+llList2String(whitelist,i)+"=add");
}

default
{
    state_entry()
    {
        //llSetMemoryLimit(16000);
        Service_Start();
    }
    attach(key kId)
    {
        if(kId)
            Start_RLV();
    }
    link_message(integer iLink,integer iNum,string Str,key kId)
    {
        if(iNum == API_SVC_STOP && Str == llGetScriptName())
        {
            Service_Stop();

            llSetScriptState(llGetScriptName(),FALSE);
            llResetScript();
        }
    }
}
