/****************************************************
_KDC Powertool HUD 2.0.lslp

Copyright (c) 2016-2018, KDC Kyrah Design Concept.

SPDX-License-Identifier: GPL-2.0-only
*****************************************************

This is the HUD portion of the powertool
Put it in a HUD prim.

Use this for scripts that should only play sound to
yourself, or whose sound usage isn't too critical and
can use the API call to the player.

If you play sounds on HUDs, only you can hear it.
****************************************************/

$import API.lslm () API_;
$import _MenuMaker.lslm () MENU_;
$import _timer.lslm () TIMER_;

key isAttached;

string TRIGGER  = ".trigger";
string SERVICE  = ".service";
list triggers;
list services;
list extra_buttons;

ResyncAddons()
{
    triggers = [];
    services = [];

	integer i;
    integer max = llGetInventoryNumber(INVENTORY_SCRIPT);
    for(i=0;i<max;i++)
    {
        string scriptname = llGetInventoryName(INVENTORY_SCRIPT,i);

        if(llGetSubString(scriptname,-8,-1) == TRIGGER)
            triggers += [llDeleteSubString(scriptname,-8,-1)];
        else if(llGetSubString(scriptname,-8,-1) == SERVICE)
            services += [llDeleteSubString(scriptname,-8,-1)];
    }
    llMessageLinked(LINK_THIS,API_SVC_RESET_MENU,"","");
}

string BTN_SERVICES = "<SERVICES>";
string BTN_PLAYER   = "<PLAYER>";
string BTN_BACK   = "<BACK>";

integer TIMER_MENU = 5655;

integer MENU_CONTEXT_ROOT = 0;
integer MENU_CONTEXT_SERVICES = 1;

RootMenu(integer page)
{
    integer i;
    integer max = llGetListLength(services);
    integer running;
    for(i=0;i<max;i++)
    {
        string script = llList2String(services,i);
        if(llGetScriptState(script+SERVICE))
            running++;
    }
    list details = llGetObjectDetails(llGetKey(),[OBJECT_SCRIPT_MEMORY,OBJECT_RUNNING_SCRIPT_COUNT,OBJECT_TOTAL_SCRIPT_COUNT]);
    integer memory = llList2Integer(details,0);
    integer scripts = llList2Integer(details,1);
    integer total = llList2Integer(details,2);

    string message =   "HUD Menu:\n\nServices running: "+(string)running+"/"+(string)max+
                       "\nMemory used: "+(string)(memory/1000)+"Kb"+
                       "\nScripts: "+(string)scripts+"/"+(string)total;
    MENU_Create(llGetOwner(),MENU_MakePage([BTN_SERVICES,BTN_PLAYER]+triggers+extra_buttons,page),message,MENU_CONTEXT_ROOT,page);
    TIMER_SetTimer(TIMER_MENU,120.0);
}

ServiceMenu(integer page)
{
	list service_names;
	integer max = llGetListLength(services);
	integer i;
	for(i=0;i<max;i++)
	{
		string script = llList2String(services,i);
        if(llGetScriptState(script+SERVICE))
            service_names += ["☑"+script];
        else
            service_names += ["☐"+script];
	}
    MENU_Create(llGetOwner(),MENU_MakePage([BTN_BACK]+service_names,page),"Toggle services on and off:",MENU_CONTEXT_SERVICES,page);
    TIMER_SetTimer(TIMER_MENU,120.0);
}
default
{
    state_entry()
    {
        llListen(API_CommChannel,"","","");
        ResyncAddons();
    }
    attach(key id)
    {
        isAttached = id;
    }
    changed(integer change)
    {
        if(change & CHANGED_INVENTORY)
            ResyncAddons();
    }
    touch_start(integer num)
    {
        if(llDetectedKey(0) != llGetOwner())
            return;
        RootMenu(0);
    }
    timer()
    {
        if(TIMER_IsItTime(TIMER_MENU) && TIMER_IsTimerPresent(TIMER_MENU))
        {
            MENU_Destroy();
            TIMER_ClearTimer(TIMER_MENU);
        }
    }
    link_message(integer snum,integer num,string str,key id)
    {
        if(num == API_SVC_ADD_MENU)
        {
            if(llListFindList(extra_buttons,[str]) != -1)
                return;
            extra_buttons+=[str];
        }
        if(num == API_SVC_REM_MENU)
        {
            integer index = llListFindList(extra_buttons,[str]);
            if(index == -1)
               return;
            extra_buttons = llDeleteSubList(extra_buttons,index,index);
        }
        else if(num == API_ROOT_MENU)
            RootMenu(0);
    }
    listen(integer channel,string name,key id,string data)
    {
        if(id != llGetOwner() && llGetOwnerKey(id) != llGetOwner())
            return;

        if(channel == API_CommChannel)
        {
            list message = llCSV2List(data);
            integer idx;
            if(llList2String(message,idx++) != API_CMD_HEADER)
                return;
            string command = llList2String(message,idx++);
            if(command == API_CMD_HUDMENU)
                RootMenu(0);
        }
        else if(channel == MENU_channel)
        {
            MENU_Destroy();
            TIMER_ClearTimer(TIMER_MENU);
            integer context = MENU_GetA();
            if(context == MENU_CONTEXT_ROOT)
            {
                if(data == MENU_BTN_PREV())
                {
                   integer page = MENU_GetB()-1;
                   if(page <0)
                       page = 0;
                    RootMenu(page);
                }
                else if(data == MENU_BTN_NEXT())
                    RootMenu(MENU_GetB()+1);
                else if(data == BTN_PLAYER)
                    API_OpenPlayer();
                else if(data == BTN_SERVICES)
                    ServiceMenu(0);
                else if(llListFindList(triggers,[data]) != -1)
                {
                    string triggerscript = data + TRIGGER;
                    if(llGetInventoryType(triggerscript) == INVENTORY_SCRIPT)
                        llSetScriptState(triggerscript,TRUE);
                    RootMenu(0);
                }
                else if(llListFindList(extra_buttons,[data]) != -1)
                    API_CallButton(data);
            }
            else if(context == MENU_CONTEXT_SERVICES)
            {
                if(data == MENU_BTN_PREV())
                {
                    integer page = MENU_GetB()-1;
                    if(page <0)
                        page = 0;
                    ServiceMenu(page);
                }
                else if(data == MENU_BTN_NEXT())
                    ServiceMenu(MENU_GetB()+1);
                else if(data == BTN_BACK)
                    RootMenu(0);
                else
                {
                    data = llDeleteSubString(data,0,0);

                    //if(llListFindList(services,[data]) == -1);
                    //    return;

                    string scriptname = data+SERVICE;
                    if(llGetScriptState(scriptname))
                        API_Service_Stop(scriptname);
                    else
                        llSetScriptState(scriptname,TRUE);

                    llSleep(1.0); //so the menu doesn't reload before the script has stopped.
                    ServiceMenu(MENU_GetB());
                }
            }
        }
    }
}
