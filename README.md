The KDC powertool is a simple script container/API I've been using for years for
personal utility scripts.

# How to use it

Click the powertool hud, the main menu should display all the available .trigger
and .service buttons.

* SERVICE: Allows you to toggle .service scripts on and off.
* PLAYER: Will open the powertool player's interface.
* BACK: Will bring you back to the previous menu.

# Triggers

Any script whose name ends with .trigger will be added to the menu of the
hud/player it has been added to.
Trigger scripts don't run unless they have been triggered, they are ment for
"one-off" commands. Honk.trigger is a good example.
Because they don't run all the time, they are fairly efficient but It's a good
idea to limit their memory to the minimum too.

# Services

Any script whose name ends with .service will be added to the SERVICE or ADDON
menu.

* Services can keep running in the background.
* Services can also register/unregister main menu buttons.
* Check Example.service.

# Addons

They are really just services.
But the player will not load ".service", only ".addon" it's just there to
differenciate them.

# Building this thing

The HUD script is ment to be in the root prim of a HUD element, clicking it
will open the main menu of the powertool.

The "player" script is ment to be worn in the root prim of a regular attachment,
It has the same function as the HUD script but doesn't respond to clicks
directly.

# Compiling

I use the LSLForge extension for Eclipse for script pre-processing and keeping
my sanity, you will need those to get SL-ready scripts out of this.

* <https://www.eclipse.org/>
* <https://github.com/elnewfie/lslforge>

# Do i need this?
You don't, it's mostly a scripter toy, it's also badly coded and I literally
rewrote it overnight. It probably has bugs and is not really good for you if you
can't script.